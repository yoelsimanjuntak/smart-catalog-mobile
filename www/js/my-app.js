var $$ = Dom7;
var app = new Framework7({
  // App root element
  el: '#app',
  theme:'md',
  // App Name
  name: 'Smart Catalog',
  // App id
  id: 'io.partopitao.smartcatalog',
  // Enable swipe panel
  panel: {
    swipe: true,
  },
  routes: routes
});
var viewHome = app.views.create('#view-home', {name:'home', url:'/', main:true});
var viewCatalog = app.views.create('#view-catalog', {name:'catalog', url:'/catalog/'});
var viewOrder = app.views.create('#view-order', {name:'order',url:'/order/'});

var searchbar = app.searchbar.create({ el:"#search-catalog" });

$(document).on('click', '.route-tab-link', function (e) {
  var url = $(this).attr('href');
  var view = app.views.get($(this).attr('data-view'));
  var tabId = $(this).attr('data-tab');
  app.tab.show(tabId);
  view.router.navigate(url);
});

viewHome.on('view:init', function(){});
viewCatalog.on('view:init', function(){});
viewOrder.on('view:init', function(){});

app.on('cartUpdated', function () {
  var cart = getLocalStorage('cart');
  if(cart && cart.length > 0) {
    $$('.badge[data-label=num-cart]').html(cart.length);
  } else {
    $$('.badge[data-label=num-cart]').html('0');
  }
});

$$("#view-order" ).on('tab:show', function(ui) {
  var auth = getLocalStorage('auth');
  if (auth) {
    getData('orders',{UserName: auth.Email}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        drawOrders($('[data-label=list-order]', ui.target), res.data);
      }
    }, function(){}, function(){});
    /*getData('projects',{UserName: auth.Email}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        setLocalStorage('projects', res.data);
        drawProjects($('[data-label=list-project]', ui.target), 'projects',5);
      }
    }, function(){}, function(){});*/
  } else {

  }
});

$$(document).on('page:init', '.page[data-name="login"]', function (e, page) {
  var el = page.el;
  $('.button-login', el).unbind('click').click(function(){
    var username_ = $$('[name=username]', el).val();
    var password_ = $$('[name=password]', el).val();

    app.preloader.show();
    getData('login', {username: username_, password: password_}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        setLocalStorage('auth', res.data);
        app.view.current.router.navigate('/',{reloadCurrent: true,ignoreCache: true,});
      }
      else {
        app.dialog.alert(res.error);
      }
    }, function() {
      app.dialog.alert("Terjadi kesalahan. Silakan coba kembali atau hubungi administrator.");
    }, function() {
      app.preloader.hide();
    });
  });
});
$$(document).on('page:init', '.page[data-name="register"]', function (e, page) {
  var el = page.el;
  $('.button-submit', el).unbind('click').click(function(){
    var _name = $$('[name=name]', el).val();
    var _phoneno = $$('[name=phoneno]', el).val();
    var _address = $$('[name=address]', el).val();
    var _password = $$('[name=password]', el).val();
    var _passwordcf = $$('[name=confirmpassword]', el).val();

    var valid = app.input.validateInputs($$('form', el));
    if(!valid) {
      app.dialog.alert('Harap isi form terlebih dahulu.');
      return false;
    }

    if(_password != _passwordcf) {
      app.dialog.alert('Harap konfirmasi password dengan benar.');
      return false;
    }

    app.preloader.show();
    getData('register', {name: _name, phoneno: _phoneno, password: _password, address: _address}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        var toast = app.toast.create({
          text: 'Registrasi Berhasil',
          icon: '<i class="icon material-icons md-only">check_circle</i>',
          position: 'center',
          closeTimeout: 2000,
          on: {
            closed: function () {
              app.view.current.router.navigate('/login/',{reloadCurrent: true,ignoreCache: true,});
            }
          }
        });
        toast.open();
      }
      else {
        app.dialog.alert(res.error);
      }
    }, function() {
      app.dialog.alert("Terjadi kesalahan. Silakan coba kembali atau hubungi administrator.");
    }, function() {
      app.preloader.hide();
    });
  });
});

$$(document).on('page:afterin', '.page[data-name="home"]', function (e, page) {
  app.emit('cartUpdated');
  var el = page.el;
  var auth = getLocalStorage('auth');

  if (auth) {
    $$('.el-hide-login', el).hide();
    $$('.el-show-login', el).show();

    $$('[data-label=name]', el).html(auth.Nama);
    $$('[data-label=email]', el).html(auth.Email);
  } else {
    $$('.el-hide-login', el).show();
    $$('.el-show-login', el).hide();

    $$('[data-label=name]', el).html('Selamat Datang!');
    $$('[data-label=email]', el).html('Silakan login / daftar akun anda.');
  }

  drawProducts($$('[data-label=list-catalog]', el), 'featured');
  getData('products-featured/2', {}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      setLocalStorage('featured', res.data);
      drawProducts($$('[data-label=list-catalog]', el), 'featured');
    }
  }, function() {

  }, function() {

  });

  drawArticles($$('[data-label=list-news]', el), 'news');
  getData('posts/1', {}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      setLocalStorage('news', res.data);
      drawArticles($$('[data-label=list-news]', el), 'news');
    }
  }, function() {

  }, function() {

  });

  $('.btn-logout', el).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin logout dari aplikasi?', 'Logout', function(){
      localStorage.clear();
      app.view.current.router.navigate('/login/',{reloadCurrent: true,ignoreCache: true,});
    }, function(){

    });
    return false;
  });
});

$$(document).on('page:afterin', '.page[data-name="catalog"]', function (e, page) {
  app.emit('cartUpdated');
  var el = page.el;

  drawProducts($$('[data-label=list-catalog]', el));
  getData('products/2', {}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      setLocalStorage('products', res.data);
      drawProducts($$('[data-label=list-catalog]', el));
    }
  }, function() {

  }, function() {

  });
});
$$(document).on('page:afterin', '.page[data-name="product"]', function (e, page) {
  app.emit('cartUpdated');
  var el = page.el;
  var param = page.route.params;
  var products = getLocalStorage('products');
  var auth = getLocalStorage('auth');

  var stepper = app.stepper.create({el: '.stepper' });
  //var _price = 0;
  var _name = 'Produk';
  var _id = 0;

  if(products) {
    var det = products.find(x => x.PostID == param.id);
    if(det) {
      $$('[data-label=title]', el).removeClass('skeleton-text skeleton-effect-wave').html(det.PostTitle);
      $$('[name=price-text]', el).val(numeral(det.PostPrice).format('0,0'));
      $$('[name=price]', el).val(parseFloat(det.PostPrice));
      //_price = parseFloat(det.PostPrice);
      _name = det.PostTitle;
      _id = det.PostID;
    }
  }

  $$('[name=qty],[name=price]', el).change(function() {
    var qty = $('[name=qty]', page.el).val();
    var price = $('[name=price]', page.el).val();

    $$('[name=total]', el).val(numeral(price*qty).format('0,0'));
  }).trigger('change');

  app.preloader.show();
  getData('product/'+param.id, {}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      $$('[data-label=title]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.PostTitle);
      $$('[data-label=desc]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.PostContent);
      $$('[data-label=img]', el).attr("src", res.data.Thumbnail);

      $$('[name=price-text]', el).val(numeral(res.data.PostPrice).format('0,0'));
      $$('[name=price]', el).val(parseFloat(res.data.PostPrice)).trigger('change');
      //_price = parseFloat(res.data.PostPrice);
      _name = res.data.PostTitle;
      _id = res.data.PostID;

      if(res.data.PostCategoryID!=1) {
        $$('[data-label=form-product]', el).show();
      } else {
        $$('[data-label=form-product]', el).hide();
      }
    }
  }, function() {

  }, function() {
    app.preloader.hide();
  });

  $('[data-label=btn-cart]', page.el).unbind('click').click(function(){
    if(!auth) {
      app.dialog.alert('Harap login terlebih dahulu untuk lanjut berbelanja.');
      return false;
    }
    var _qty = $('[name=qty]', page.el).val();
    var _total = $$('[name=total]', page.el).val();
    var _price = $$('[name=price]', page.el).val();
    app.dialog.confirm('Apakah anda yakin ingin menambah ke keranjang? <br /><br /> <strong>'+_name+'</strong> x <strong>'+_qty+'</strong> = <strong>'+_total+'</strong>', 'Tambah Keranjang', function(){
      var cartObj = {PostID: _id, PostTitle: _name, Qty: _qty, Price: _price, Total: _total};
      pushCart(cartObj);
      app.emit('cartUpdated');
      app.view.current.router.back('',{force: true});
    }, function(){

    });
  });
});

$$(document).on('page:afterin', '.page[data-name="cart"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
  var auth = getLocalStorage('auth');

  app.preloader.show();
  drawCart($$('[data-label=list-cart]', el));
  app.preloader.hide();

  $('[data-label=input-address]', el).val(auth.Address);

  $('[data-label=btn-cart-remove]', el).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin menghapus isi keranjang anda?', 'Hapus Keranjang', function(){
      removeLocalStorage('cart');
      app.emit('cartUpdated');
      app.view.current.router.refreshPage();
    }, function(){

    });
    return false;
  });

  $('[data-label=btn-checkout]', el).unbind('click').click(function(){
    if(!auth) {
      app.dialog.alert('Harap login terlebih dahulu!');
      return false;
    }

    app.dialog.confirm('Apakah anda yakin ingin memproses pembelian? Pastikan data yang anda input sudah benar.', 'Checkout', function(){
      var carts = getLocalStorage('cart');
      var address_ = $('[data-label=input-address]', el).val();
      var remarks_ = $('[data-label=input-remarks]', el).val();
      var total_ = 0;

      if(carts.length==0) {
        app.dialog.alert('Keranjang belanja masih kosong!');
        return false;
      }
      if(!address_) {
        app.dialog.alert('Harap isi alamat pengiriman!');
        return false;
      }

      for(var i=0; i<carts.length; i++) {
        total_ += parseFloat(carts[i].Qty)*parseFloat(carts[i].Price);
      }

      var input = {
        OrderName: auth.Nama,
        OrderPhone: auth.Email,
        OrderAddress: address_,
        OrderStatus: 'MENUNGGU',
        OrderTotal: total_,
        OrderItems: JSON.stringify(carts),
        OrderRemarks: remarks_,
        UserName: auth.Email
      };
      app.preloader.show();
      getData('order-add/', input, function(res) {
        res = JSON.parse(res);
        if(res.error == 0) {
          removeLocalStorage('cart');
          app.dialog.alert(res.success);
          app.view.current.router.back('',{force: true});
          app.tab.show('#view-order');
          app.toolbar.show('[data-label=tabbar-main]');
          app.view.current.router.navigate('/order/'+res.data.OrderID+'/',{reloadCurrent: true,ignoreCache: true,});
        } else {
          app.dialog.alert(res.error);
          return false;
        }
      }, function(){

      }, function(){
        app.preloader.hide();
      });
    }, function(){

    });
    return false;
  });
});

$$(document).on('page:afterin', '.page[data-name="order-detail"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
  var auth = getLocalStorage('auth');

  app.preloader.show();
  getData('order/'+param.id, {}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      $$('[data-label=title]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.OrderID);
      $$('[data-label=order-no]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.OrderID.padStart(4, '0'));
      $$('[data-label=order-status]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.OrderStatus);
      $$('[data-label=order-date]', el).removeClass('skeleton-text skeleton-effect-wave').html(moment(res.data.CreatedOn).format('DD/MM/YYYY'));
      $$('[data-label=order-total]', el).removeClass('skeleton-text skeleton-effect-wave').html('<strong>'+numeral(res.data.OrderTotal).format('0,0')+'</strong>');

      var arrItem = JSON.parse(res.data.OrderItems);
      var txtItem = '<ul>';
      if(arrItem.length>0) {
        for(var i=0; i<arrItem.length; i++) {
          txtItem +='<li>';
          txtItem +='<div class="item-content">';
          txtItem +='<div class="item-inner">';
          //txtItem +='<div class="item-title-row">';
          txtItem +='<div class="item-title">'+arrItem[i].PostTitle+'<div class="item-footer">'+arrItem[i].Qty+' x '+numeral(arrItem[i].Price).format('0,0')+'</div></div>';
          //txtItem +='</div>';
          //txtItem +='<div class="item-subtitle">_____</div>';
          //txtItem +='<div class="item-text">_____ __ _____</div>';
          txtItem +='<div class="item-after"><span class="text-color-primary">'+arrItem[i].Total+'</span></div>';
          txtItem +='</div>';
          txtItem +='</div>';
          txtItem +='</li>';
        }
      } else {
        txtItem += '<li class="text-align-center" style="padding: 10px !important">KOSONG</li>';
      }
      txtItem += '</ul>';
      $$('[data-label=list-item]', el).removeClass('skeleton-text skeleton-effect-wave').html(txtItem);
      $$('[data-label=bank-info]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.BankInfo);
    }
  }, function() {

  }, function() {
    app.preloader.hide();
  });

});
