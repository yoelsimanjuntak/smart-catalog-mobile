routes = [
  /*{
    path: '/tabs/',
    tabs: [
      {
        path: '/',
        id: 'view-home'
      },
      {
        path: '/catalog/',
        id: 'view-catalog'
      },
      {
        path: '/order/',
        id: 'view-order'
      },
    ],
  },*/
  {
    name:'home',
    path: '/',
    url: './index.html'
  },
  {
    name:'login',
    path: '/login/',
    url: './pages/login.html'
  },
  {
    name:'register',
    path: '/register/',
    url: './pages/register.html'
  },
  {
    name:'product',
    path: '/product/:id/',
    url: './pages/product.html'
  },
  {
    name:'article',
    path: '/article/:id/',
    url: './pages/article.html'
  },
  {
    name:'cart',
    path: '/cart/',
    url: './pages/cart.html'
  },
  {
    name:'order',
    path: '/order-detail/:id/',
    url: './pages/order.html'
  },
  // Default route (404 page). MUST BE THE LAST
  /*{
      path: '(.*)',
      url: './pages/404.html'
  },*/
];
