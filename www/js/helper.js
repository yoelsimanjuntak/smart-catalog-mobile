/**
 * Created by Toshiba on 5/11/2019.
 */
//var URL_API = "http://localhost/web-smartcatalog/site/api/";
//var URL_BASE = "http://localhost/web-smartcatalog/";
var URL_API = "https://chayrasmart.com/catalog/site/api/";
var URL_BASE = "https://chayrasmart.com/catalog/";

function isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

function setLocalStorage(key, val) {
  localStorage.setItem(key, JSON.stringify(val));
}

function getLocalStorage(key) {
  var ls = localStorage.getItem(key);
  if(isJson(ls)) {
    return JSON.parse(ls);
  }
  else {
    return ls;
  }
}

function removeLocalStorage(key) {
    localStorage.removeItem(key);
}

function getData(action, data, successCB, failCB, doneCB) {
  var url = URL_API + action;
  $.ajax({
    type: "POST",
    url: url,
    data: data,
    crossDomain: true,
    success: successCB,
    error: failCB,
    complete: function() {
      if(doneCB) (doneCB)();
    }
  });
}

function drawProducts(el, item='products') {
  //console.log('draw', el);
  var prods = getLocalStorage(item);
  el.empty();
  el.removeClass('skeleton-text skeleton-effect-wave');

  var html = '<ul>';
  if(prods && prods.length>0) {
    for(var i=0; i<prods.length; i++) {
      html += '<li>';
      html += '<a href="/product/'+prods[i].PostID+'/" class="item-link item-content">';
      html += '<div class="item-media">';
      html += '<img src="'+(prods[i].Thumbnail?prods[i].Thumbnail:'img/icon-skeleton.png')+'" width="80" height="80"/>';
      html += '</div>';
      html += '<div class="item-inner">';
      html += '<div class="item-title-row">';
      html += '<div class="item-title">'+prods[i].PostTitle+'</div>';
      html += '</div>';
      html += '<div class="item-subtitle">'+prods[i].PostCategoryName+'</div>';
      html += '<div class="item-text">Rp. '+numeral(prods[i].PostPrice).format('0,0')+'</div>';
      html += '</div>';
      html += '</a>';
      html += '</li>';
    }
  } else {
    html += '<li class="item-divider text-align-center">Maaf, belum ada data tersedia saat ini.</li>';
  }
  html += '</ul>';
  el.html(html);
}

function drawArticles(el, item='news') {
  //console.log('draw', el);
  var prods = getLocalStorage(item);
  el.empty();
  el.removeClass('skeleton-text skeleton-effect-wave');

  var html = '<ul>';
  if(prods && prods.length>0) {
    for(var i=0; i<prods.length; i++) {
      html += '<li>';
      html += '<a href="/product/'+prods[i].PostID+'/" class="item-link item-content">';
      html += '<div class="item-media">';
      html += '<img src="'+(prods[i].Thumbnail?prods[i].Thumbnail:'img/icon-skeleton.png')+'" width="80" height="80"/>';
      html += '</div>';
      html += '<div class="item-inner">';
      html += '<div class="item-title-row">';
      html += '<div class="item-title">'+prods[i].PostDate+'</div>';
      html += '</div>';
      //html += '<div class="item-subtitle">'+prods[i].PostDate+'</div>';
      html += '<div class="item-text">'+prods[i].PostTitle+'</div>';
      html += '</div>';
      html += '</a>';
      html += '</li>';
    }
  } else {
    html += '<li class="item-divider text-align-center">Maaf, belum ada data tersedia saat ini.</li>';
  }
  html += '</ul>';
  el.html(html);
}

function drawCart(el, item='cart') {
  //console.log('draw', el);
  var prods = getLocalStorage(item);
  el.empty();
  el.removeClass('skeleton-text skeleton-effect-wave');

  var html = '<ul>';
  if(prods && prods.length>0) {
    var sum = 0;
    for(var i=0; i<prods.length; i++) {
      html += '<li>';
      html += '<a href="/product/'+prods[i].PostID+'/" class="item-link item-content">';
      html += '<div class="item-inner">';
      html += '<div class="item-title">'+prods[i].PostTitle+'<div class="item-footer">'+prods[i].Qty+' x '+numeral(prods[i].Price).format('0,0')+'</div></div>';
      html += '<div class="item-after"><span class="text-color-primary">'+prods[i].Total+'</span></div>';
      html += '</div>';
      html += '</a>';
      html += '</li>';
      sum += (parseFloat(prods[i].Qty)*prods[i].Price);
    }
    html += '<li>';
    html += '<div class="item-content">';
    html += '<div class="item-inner">';
    html += '<div class="item-title"><strong>Total : '+numeral(sum).format('0,0')+'</strong></div>';
    html += '</div>';
    html += '</div>';
    html += '</li>';
  } else {
    html += '<li class="item-divider text-align-center">Maaf, belum ada data tersedia saat ini.</li>';
  }
  html += '</ul>';
  el.html(html);
}

function pushCart(obj) {
  var cart = getLocalStorage('cart');
  if (!cart) {
    cart = [];
  }
  cart.push(obj);
  setLocalStorage('cart', cart);
}

function drawOrders(el, items) {
  console.log('draw', el);
  console.log('items', items);
  var prods = items;
  el.empty();
  el.removeClass('skeleton-text skeleton-effect-wave');

  var html = '<ul>';
  if(prods && prods.length>0) {
    for(var i=0; i<prods.length; i++) {
      var arrItem = JSON.parse(prods[i].OrderItems);
      var txtItem = '';

      for(var x=0; x<arrItem.length; x++) {
        txtItem += arrItem[x].PostTitle+' x '+arrItem[x].Qty;
        if(arrItem[x+1]) {
          txtItem += ', ';
        }
      }
      html += '<li>';
      html += '<a href="/order-detail/'+prods[i].OrderID+'/" class="item-link item-content">';
      html += '<div class="item-inner">';
      html += '<div class="item-title"><div class="item-header">'+prods[i].OrderStatus+'</div><strong>'+prods[i].OrderID.padStart(4, '0')+'</strong><div class="item-footer">'+txtItem+'</div></div>';
      html += '<div class="item-after"><span class="text-color-primary">'+numeral(prods[i].OrderTotal).format('0,0')+'</span></div>';
      html += '</div>';
      html += '</a>';
      html += '</li>';
    }
  } else {
    html += '<li class="item-divider text-align-center">Maaf, belum ada data tersedia saat ini.</li>';
  }
  html += '</ul>';
  el.html(html);
}
